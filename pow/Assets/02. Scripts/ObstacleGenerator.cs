﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ObstacleGenerator : MonoBehaviour {

	public GameObject purpleObstacle;
	public GameObject brownObstacle;
	public List<GameObject> obstacleList = new List<GameObject>();

	public GameObject player;

	private float partialTimeAcum;
	private float totalTimeAcum;

	//Debug purposes only
	public bool showSpawnTimePeaks = true;
	private bool isIncreasing = true;
	private float prevTimer;

	private float spawnTimer;

	public float initialObsPerMinute = 20f;
	public float difficultyDecreaser = 3f;
	public int calmAngle;
	public float obstacleSeparation;


	// Use this for initialization
	void Start() {
		
		partialTimeAcum = 0.0f;
		prevTimer = 0.0f;
		spawnTimer = 60 / initialObsPerMinute;
	}
	
	// Update is called once per frame
	void Update() {
	
		partialTimeAcum += Time.deltaTime;
		totalTimeAcum += Time.deltaTime;
		
		prevTimer = spawnTimer;
		spawnTimer = spawnRateCalculator();
		
		if (showSpawnTimePeaks)
			{
				debugSpawnTime();
			}

		if (partialTimeAcum >= spawnTimer)
			{
				createObstacle();
				partialTimeAcum = 0.0f;
			}
	}

	// Mostra canvis de tendencia en el temps d'spawn
	private void debugSpawnTime() {

		// cas spawn time es maxim
		if (prevTimer > spawnTimer && isIncreasing)
			{
				isIncreasing = false;
				//Debug.Log("SpawnTimer : " + prevTimer);
			} 
			// cas spawn time és mínim
			else if (prevTimer < spawnTimer && !isIncreasing)
			{
				isIncreasing = true;
				//Debug.Log("SpawnTimer : " + prevTimer);
			}

	}

	void createObstacle() {
		
		int rotationAngle = generateAngle();

		Vector3 playerRot = player.gameObject.transform.rotation.eulerAngles;
		Vector3 spawnRot = new Vector3(0, 0, rotationAngle);

		GameObject obstacle = (GameObject)Instantiate(generateRandomObstacle(), Vector3.zero, Quaternion.Euler(spawnRot));
		obstacleList.Add(obstacle);
	}

	float spawnRateCalculator() {
			
		// 20+(x/3*log(x/6))+x/6*sin(x/9)
		spawnTimer = initialObsPerMinute +
		(totalTimeAcum / difficultyDecreaser) * Mathf.Log(totalTimeAcum / difficultyDecreaser * 2) +
		(totalTimeAcum / difficultyDecreaser * 2) * Mathf.Sin(totalTimeAcum / (difficultyDecreaser * 3));


		//Debug.Log("spawnTime : " + (60 / spawnTimer));

		return 60 / spawnTimer;
	}

	int generateAngle() {

		int angle = 0;
		float playerZAngle = player.gameObject.transform.rotation.eulerAngles.z;

		float minLowerAngle;
		float maxHigherAngle;


		minLowerAngle = playerZAngle - calmAngle;
		maxHigherAngle = playerZAngle + calmAngle;
					
		while (angle == 0)
			{					
				angle = Random.Range(0, 360);

				if (angle > minLowerAngle && angle < maxHigherAngle
				    || angle > (minLowerAngle + 360) && angle < maxHigherAngle + 360
				    || angle > (minLowerAngle - 360) && angle < maxHigherAngle - 360)
					{
						//Debug.Log("Continue, massa a prop del jugador: " + playerZAngle + "/" + angle);
						angle = 0;
						continue;
					}

				//Intentem generar obstacles amb un mínim de separació entre ells
				//En cas de no poder colocarlo ho tornem a intentar disminuint la separació entre mínima entre aquests
				float obstacleAngle;
				//Debug.Log("obstacleList: " + obstacleList.Count);

				foreach (GameObject obstacle in obstacleList)
					{
						obstacleAngle = obstacle.gameObject.transform.rotation.eulerAngles.z;
						if (obstacleList.Count > 0)
							{
								obstacleSeparation = 90 / obstacleList.Count;
							}
						Debug.Log("obstacleSeparation: " + obstacleSeparation);

						if (angle > obstacleAngle - obstacleSeparation && angle < obstacleAngle + obstacleSeparation)
							{
								//Debug.Log("Continue, massa a prop d'un altre obstacle: " + angle);
								angle = 0;
								continue;
							}
					}

			}
			
		//Debug.Log("obstacleSeparation: " + obstacleSeparation);
		Debug.Log(minLowerAngle + "<" + angle + "<" + maxHigherAngle);
		
		return angle;
	}

	private GameObject generateRandomObstacle() {

		int state = Random.Range(0, 2);
		GameObject obstacle;

		if (state == 0)
			{
				obstacle = brownObstacle;
			} else
			{
				obstacle = purpleObstacle;				
			}
			
		return obstacle;
	}

	public bool destroyObstacle(GameObject obstacle) {

		GameObject tempObstacle = obstacleList.Find(obs => obs.transform.rotation.eulerAngles.z == obstacle.transform.rotation.eulerAngles.z);

		bool output = obstacleList.Remove(tempObstacle);
		Destroy(obstacle);
		Destroy(obstacle.transform.parent.gameObject);

		return output;
	}
}
