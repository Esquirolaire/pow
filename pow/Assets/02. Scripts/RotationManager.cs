﻿using UnityEngine;
using System.Collections;

public class RotationManager : MonoBehaviour {

	public float speedRotation = 20.0f;

	private 

	// Use this for initialization
	void Start() {
	
	}
	
	// Update is called once per frame
	void Update() {

		Vector3 rot = new Vector3(0, 0, speedRotation * Time.deltaTime);
		transform.Rotate(rot);
	}
}
