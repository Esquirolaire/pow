﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Player : MonoBehaviour {

	public Sprite stateBrown;
	public Sprite statePurple;

	public ObstacleGenerator obstacleGenerator;

	public Text scorePanel;
	public Text timePanel;
	private int score;
	private float timer = 0f;

	private bool isStateBrown;

	// Use this for initialization
	void Start() {
		timer = 0f;
		score = 0;
		isStateBrown = true;
		GetComponent<SpriteRenderer>().sprite = stateBrown;
	}
	
	// Update is called once per frame
	void Update() {
		#if UNITY_IPHONE || UNITY_ANDROID
			if (Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch((0).phase = TouchPhase == TouchPhase.Ended) {
				isStateBrown = !isStateBrown;
			}		
		#else
		if (Input.anyKeyDown)
			{
				isStateBrown = !isStateBrown;
			}
		#endif

		if (isStateBrown)
			{
				GetComponent<SpriteRenderer>().sprite = stateBrown;
			} else
			{
				GetComponent<SpriteRenderer>().sprite = statePurple;
			}


		scorePanel.text = "Score: " + score;
		timer += Time.deltaTime;
		timePanel.text = "Time: " + (int)timer;
		

	}

	void OnTriggerEnter2D(Collider2D other) {
	
		if (other.gameObject.name.Equals("obsMarron") && isStateBrown
		    || other.gameObject.name.Equals("obsLila") && !isStateBrown)
			{
				score++;
				obstacleGenerator.destroyObstacle(other.gameObject);				
			} else
			{
				Destroy(this.gameObject);
				PlayerPrefs.SetInt("score", score);
				PlayerPrefs.SetInt("time", (int)timer);
				SceneManager.LoadScene("Main_Scene");
			}
	}
}
