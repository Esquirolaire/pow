﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIScript : MonoBehaviour {

	public Text scoreText;
	public Text timeText;

	// Use this for initialization
	void Start() {

		int score = PlayerPrefs.GetInt("score", 0);
		int time = PlayerPrefs.GetInt("time", 0);

		scoreText.text = "Score: " + score;
		timeText.text = "Time: " + time;
	}
	
	// Update is called once per frame
	void Update() {
	
	}

	public void Go2Scene(string nameScene) {
		SceneManager.LoadScene(nameScene);
	}
}
